﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGHeroes
{
    public class Armor : Item
    {
        //the types of armor
        public enum ArmorType
        {
            Cloth,
            Leather,
            Mail,
            Plate
        }


        public ArmorType armorType;
        public string ArmorName;

        // constructor of the armor class
        public Armor(ArmorType armorType, string ArmorName)
        {
            ArmorAttribute = new HeroAttribute();
            this.armorType = armorType;
            this.ArmorName = ArmorName;

            switch (armorType)
            {
                case ArmorType.Cloth:
                    ArmorAttribute.Strength = 1;
                    ArmorAttribute.Dexterity = 2;
                    ArmorAttribute.Intelligence = 1;
                    break;
                case ArmorType.Leather:
                    ArmorAttribute.Strength = 1;
                    ArmorAttribute.Dexterity = 1;
                    ArmorAttribute.Intelligence = 1;
                    break;
                case ArmorType.Mail:
                    ArmorAttribute.Strength = 3;
                    ArmorAttribute.Dexterity = 3;
                    ArmorAttribute.Intelligence = 3;
                    break;
                case ArmorType.Plate:
                    ArmorAttribute.Strength = 3;
                    ArmorAttribute.Dexterity = 5;
                    ArmorAttribute.Intelligence = 2;
                    break;
                
            }
        }

    }
}
