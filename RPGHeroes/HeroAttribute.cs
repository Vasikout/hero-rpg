﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGHeroes
{
    public class HeroAttribute
    {


        private int strength = 0;
        private int dexterity = 0;
        private int intelligence = 0;

        // properties for the 3 attributes, strength, dexterity, intelligence
        public int Strength
        {
            get
            {
                return strength;
            }

            set
            {
                strength = value;
            }
        }

        public int Intelligence
        {
            get
            {
                return intelligence;
            }

            set
            {
                intelligence = value;
            }
        }

        public int Dexterity
        {
            get
            {
                return dexterity;
            }

            set
            {
                dexterity = value;
            }
        }

        // methods for increasing the attributes
        public void IncreaseStrength(int strengthIncrease)
        {
            strength += strengthIncrease;
        }

        public void IncreaseDexterity(int dexterityIncrease)
        {
            dexterity += dexterityIncrease;
        }

        public void IncreaseIntelligence(int intelligenceIncrease)
        {
            intelligence += intelligenceIncrease;
        }


    }
}
