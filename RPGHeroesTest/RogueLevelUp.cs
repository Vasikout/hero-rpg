﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using RPGHeroes;

namespace RPGHeroesTest
{
    public class RogueLevelUp
    {
        [Fact]

        public void RogueLevelsUpCorrectlyAttributesAddedCorrectly()
        {
            Rogue rogue = new Rogue("Jack");
            rogue.LevelUp();

            int expectedStrength = 3;
            int expectedDexterity = 10;
            int expectedIntelligence = 2;
            int expectedLevel = 2;

            //Assert

            Assert.Equal(expectedStrength, rogue.LevelAttributes.Strength);
            Assert.Equal(expectedDexterity, rogue.LevelAttributes.Dexterity);
            Assert.Equal(expectedIntelligence, rogue.LevelAttributes.Intelligence);
            Assert.Equal(expectedLevel, rogue.PlayerLevel);
        }
    }
}
