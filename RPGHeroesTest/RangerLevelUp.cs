﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using RPGHeroes;

namespace RPGHeroesTest
{
    public class RangerLevelUp
    {
        [Fact]

        public void RangerLevelsUpCorrectlyAttributesAddedCorrectly()
        {
            Ranger ranger = new Ranger("John");
            ranger.LevelUp();

            int expectedStrength = 2;
            int expectedDexterity = 12;
            int expectedIntelligence = 2;
            int expectedLevel = 2;

            //Assert

            Assert.Equal(expectedStrength, ranger.LevelAttributes.Strength);
            Assert.Equal(expectedDexterity, ranger.LevelAttributes.Dexterity);
            Assert.Equal(expectedIntelligence, ranger.LevelAttributes.Intelligence);
            Assert.Equal(expectedLevel, ranger.PlayerLevel);
        }
    }
}
