﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using RPGHeroes;

namespace RPGHeroesTest
{
    public class WarriorHeroCreation
    {
        [Fact]

        public void CreationWarriorCorrectLevelNameAttributes()
        {
            Warrior warrior = new Warrior("Bill");

            HeroAttribute warriorAttr = warrior.LevelAttributes;

            //Act
            int expectedStrength = 5;
            int expectedDexterity = 2;
            int expectedIntelligence = 1;
            int expectedLevel = 1;
            string expectedName = "Bill";

            //Assert

            Assert.Equal(expectedName, warrior.Name);
            Assert.Equal(expectedLevel, warrior.PlayerLevel);
            Assert.Equal(expectedIntelligence, warriorAttr.Intelligence);
            Assert.Equal(expectedDexterity, warriorAttr.Dexterity);
            Assert.Equal(expectedStrength, warriorAttr.Strength);
        }
    }
}
