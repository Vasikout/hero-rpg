﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGHeroes
{
    public abstract class Item
    {
        public string Name { get; set; }

        private int requiredLevel;

        // the required level for equipping the items
        public int RequiredLevel
        {
            get
            {
                return requiredLevel;
            }

            set
            {
                requiredLevel = value;
            }
        }

        public HeroAttribute ArmorAttribute { get; set; }

        // the slots the items can have
        public enum Slot
        {
            Weapon,
            Head,
            Body,
            Legs
        }

    }
}
