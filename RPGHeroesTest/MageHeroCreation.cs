using System;
using Xunit;
using RPGHeroes;

namespace RPGHeroesTest
{
    public class MageHeroCreation
    {
        [Fact]
        public void CreationMageCorrectLevelNameAttributes()
        {
            Mage mage = new Mage("Jack");

            HeroAttribute mageAttr = mage.LevelAttributes;
            //Act
            int expectedStrength = 1;
            int expectedDexterity = 1;
            int expectedIntelligence = 8;
            int expectedLevel = 1;
            string expectedName = "Jack";

            //Assert

            Assert.Equal(expectedName, mage.Name);
            Assert.Equal(expectedLevel, mage.PlayerLevel);
            Assert.Equal(expectedIntelligence, mageAttr.Intelligence);
            Assert.Equal(expectedDexterity, mageAttr.Dexterity);
            Assert.Equal(expectedStrength, mageAttr.Strength);
        }
    }
}
