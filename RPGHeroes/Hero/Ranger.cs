﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGHeroes
{
    public class Ranger : Hero
    {
        public Ranger(string name)
        {
            LevelAttributes = new HeroAttribute();
            Name = name;
            LevelAttributes.Strength = 1;
            LevelAttributes.Dexterity = 7;
            LevelAttributes.Intelligence = 1;
            PlayerLevel = 1;
        }

        public List<Weapon.WeaponType> weapons = new List<Weapon.WeaponType>();
        public List<Armor.ArmorType> armors = new List<Armor.ArmorType>();


        public override void LevelUp()
        {
            PlayerLevel = PlayerLevel + 1;
            LevelAttributes.IncreaseStrength(1);
            LevelAttributes.IncreaseDexterity(5);
            LevelAttributes.IncreaseIntelligence(1);

        }

        public void EquipWeapon(Weapon weapon)
        {
            if (weapon.weaponType != Weapon.WeaponType.Bows || weapon.RequiredLevel > PlayerLevel)
            {
                throw new InvalidWeaponException("You can't equip this weapon");
            }
            else
            {
                Equipment.Add(Item.Slot.Weapon, weapon);
                Console.WriteLine("This is the correct weapon");
            };
        }

        public void EquipArmor(Armor armor)
        {
            if ((armor.armorType != Armor.ArmorType.Leather && armor.armorType != Armor.ArmorType.Mail) || armor.RequiredLevel > PlayerLevel)
            {
                throw new InvalidArmorException("You cant equip this armor");
            }
            else
            {
                Equipment.Add(Item.Slot.Body, armor);
                Console.WriteLine("This is the correct armor");
            }
        }

        public override HeroAttribute TotalAttributes()
        {
            HeroAttribute outputHeroAttribute = new HeroAttribute();
            //int DexteritySum = heroAttribute.Dexterity + armor.ArmorAttribute.Dexterity;
            //int IntelligenceSu = heroAttribute.Intelligence + armor.ArmorAttribute.Intelligence;
            //int StrengthSum = heroAttribute.Strength + armor.ArmorAttribute.Strength;
            foreach (var item in Equipment)
            {
                if (item.Key == Item.Slot.Weapon)
                {
                    continue;
                }
                outputHeroAttribute.Dexterity += item.Value.ArmorAttribute.Dexterity;
                outputHeroAttribute.Intelligence += item.Value.ArmorAttribute.Intelligence;
                outputHeroAttribute.Strength += item.Value.ArmorAttribute.Strength;
            }
            outputHeroAttribute.Dexterity += LevelAttributes.Dexterity;
            outputHeroAttribute.Intelligence += LevelAttributes.Intelligence;
            outputHeroAttribute.Strength += LevelAttributes.Strength;

            return outputHeroAttribute;

        }

        public float Damage(Weapon weapon)
        {
            HeroAttribute tempRangerAttribute = TotalAttributes();
            float RangerDamage = (1 + tempRangerAttribute.Dexterity / 100) * weapon.WeaponDamage;
            return RangerDamage;
        }

        public string Display(Weapon weapon)
        {
            HeroAttribute attributes = TotalAttributes();
            float damage = Damage(weapon);
            string display = String.Format("Name: {0}\nClass: Ranger\nPlayer level: {1}\nTotal strength: {2}\nTotal dexterity: {3}\nTotal intelligence: {4}\nDamage: {5}", Name, PlayerLevel, attributes.Strength, attributes.Dexterity, attributes.Intelligence, damage);
            return display;
        }

    }
}
