﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using RPGHeroes;

namespace RPGHeroesTest
{
    public class WarriorLevelUp
    {
        [Fact]
        public void WarriorLevelsUpCorrectlyAttributesAddedCorrectly()
        {
            Warrior warrior = new Warrior("Bill");
            warrior.LevelUp();

            int expectedStrength = 8;
            int expectedDexterity = 4;
            int expectedIntelligence = 2;
            int expectedLevel = 2;

            //Assert

            Assert.Equal(expectedStrength, warrior.LevelAttributes.Strength);
            Assert.Equal(expectedDexterity, warrior.LevelAttributes.Dexterity);
            Assert.Equal(expectedIntelligence, warrior.LevelAttributes.Intelligence);
            Assert.Equal(expectedLevel, warrior.PlayerLevel);
        }
    }
}
