﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using RPGHeroes;

namespace RPGHeroesTest
{
    public class MageLevelup
    {
        [Fact]
        public void MageLevelsUpCorrectlyAttributesAddedCorrectly()
        {
            Mage mage = new Mage("Nick");
            mage.LevelUp();

            int expectedStrength = 2;
            int expectedDexterity = 2;
            int expectedIntelligence = 13;
            int expectedLevel = 2;

            //Assert

            Assert.Equal(expectedStrength, mage.LevelAttributes.Strength);
            Assert.Equal(expectedDexterity, mage.LevelAttributes.Dexterity);
            Assert.Equal(expectedIntelligence, mage.LevelAttributes.Intelligence);
            Assert.Equal(expectedLevel, mage.PlayerLevel);


        }
    }
}
