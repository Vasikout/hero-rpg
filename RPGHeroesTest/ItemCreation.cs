﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using RPGHeroes;

namespace RPGHeroesTest
{
    public class ItemCreation
    {
        [Fact]
        public void WeaponCreationCorrectNameSlotType()
        {
            int expectedWeaponDamage = 3;
            string expectedName = "Mjolnir";
            Weapon.WeaponType expectedWeaponType = Weapon.WeaponType.Axes;
            Item.Slot expectedSlot = Item.Slot.Weapon;

            Weapon weapon = new Weapon(Weapon.WeaponType.Axes, "Mjolnir", Item.Slot.Weapon);

            Assert.Equal(expectedName, weapon.WeaponName);
            Assert.Equal(expectedWeaponDamage, weapon.WeaponDamage);
            Assert.Equal(expectedWeaponType, weapon.weaponType);
            Assert.Equal(expectedSlot, weapon.WeaponSlot);

        }

        [Fact]

        public void ArmorCreationCorrectNameSlotType()
        {
            string expectedArmorName = "Shield";
            int expectedLevel = 10;
            Armor.ArmorType expectedType = Armor.ArmorType.Cloth;
            //Item.Slot expectedSlot = Item.Slot.Body;
            int expectedStrength = 1;
            int expectedDexterity = 2;
            int expectedIntelligence = 1;

            Armor armor = new Armor(Armor.ArmorType.Cloth, "Shield");
            armor.RequiredLevel = 10;

            Assert.Equal(expectedLevel, armor.RequiredLevel);
            Assert.Equal(expectedStrength, armor.ArmorAttribute.Strength);
            Assert.Equal(expectedDexterity, armor.ArmorAttribute.Dexterity);
            Assert.Equal(expectedIntelligence, armor.ArmorAttribute.Intelligence);
            Assert.Equal(expectedArmorName, armor.ArmorName);
            //Assert.Equal(expectedSlot, armor.Item.Slot.Body);
            Assert.Equal(expectedType, armor.armorType);
        }
    }
}
