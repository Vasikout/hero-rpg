﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using RPGHeroes;

namespace RPGHeroesTest
{
    public class MageDamage
    {
        [Fact]

        public void MageDamageWithOrWithoutWeaponsAndArmor()
        {
            Mage mage = new Mage("Nick");
            double expectedDamageNoW = 0.27;
            Weapon weapon = new Weapon(Weapon.WeaponType.Staffs, "stuff", Item.Slot.Weapon);
            float actualDamage = mage.Damage(weapon);
            Assert.Equal(expectedDamageNoW, actualDamage);
            
        }
    }
}
