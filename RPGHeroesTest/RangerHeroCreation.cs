﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using RPGHeroes;

namespace RPGHeroesTest
{
    public class RangerHeroCreation
    {
        [Fact]

        public void CreationRangerCorrectLevelNameAttributes()
        {
            Ranger ranger = new Ranger("Nick");

            HeroAttribute rangerAttr = ranger.LevelAttributes;
            //Act
            int expectedStrength = 1;
            int expectedDexterity = 7;
            int expectedIntelligence = 1;
            int expectedLevel = 1;
            string expectedName = "Nick";

            //Assert

            Assert.Equal(expectedName, ranger.Name);
            Assert.Equal(expectedLevel, ranger.PlayerLevel);
            Assert.Equal(expectedIntelligence, rangerAttr.Intelligence);
            Assert.Equal(expectedDexterity, rangerAttr.Dexterity);
            Assert.Equal(expectedStrength, rangerAttr.Strength);
        }
    }
}
