﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGHeroes
{
    public class Mage : Hero
    {
        public Mage(string name)
        {
            LevelAttributes = new HeroAttribute();
            Name = name;
            LevelAttributes.Strength = 1;
            LevelAttributes.Dexterity = 1;
            LevelAttributes.Intelligence = 8;
            PlayerLevel = 1;
        }


        // method for when the mage levels up
        public override void LevelUp()
        {
            PlayerLevel = PlayerLevel + 1;
            LevelAttributes.IncreaseStrength(1);
            LevelAttributes.IncreaseDexterity(1);
            LevelAttributes.IncreaseIntelligence(5);
        }

        // method for equipping weapons
        public void EquipWeapon(Weapon weapon)
        {
            if ((weapon.weaponType != Weapon.WeaponType.Staffs && weapon.weaponType != Weapon.WeaponType.Wands) || weapon.RequiredLevel > PlayerLevel)
            {
                throw new InvalidWeaponException("You can't equip this weapon");
            }
            else
            {
                Equipment.Add(Item.Slot.Weapon, weapon);
                Console.WriteLine("This is the correct weapon");
            }
        }

        // method for equipping armor
        public void EquipArmor(Armor armor)
        {
            if (armor.armorType != Armor.ArmorType.Cloth || armor.RequiredLevel > PlayerLevel)
            {
                throw new InvalidArmorException("You cant equip this armor");
            }
            else
            {
                Equipment.Add(Item.Slot.Body, armor);
                Console.WriteLine("This is the correct armor");
            }
        }

        // method for calculating the mage's total attributes
        public override HeroAttribute TotalAttributes()
        {
            HeroAttribute outputHeroAttribute = new HeroAttribute();
            //int DexteritySum = heroAttribute.Dexterity + armor.ArmorAttribute.Dexterity;
            //int IntelligenceSu = heroAttribute.Intelligence + armor.ArmorAttribute.Intelligence;
            //int StrengthSum = heroAttribute.Strength + armor.ArmorAttribute.Strength;
            foreach (var item in Equipment)
            {
                if (item.Key == Item.Slot.Weapon)
                {
                    continue;
                }
                outputHeroAttribute.Dexterity += item.Value.ArmorAttribute.Dexterity;
                outputHeroAttribute.Intelligence += item.Value.ArmorAttribute.Intelligence;
                outputHeroAttribute.Strength += item.Value.ArmorAttribute.Strength;
            }
            outputHeroAttribute.Dexterity += LevelAttributes.Dexterity;
            outputHeroAttribute.Intelligence += LevelAttributes.Intelligence;
            outputHeroAttribute.Strength += LevelAttributes.Strength;

            return outputHeroAttribute;

        }

        // method for calculating the mage's damage
        public float Damage(Weapon weapon)
        {
            HeroAttribute tempMageAttribute = TotalAttributes();
            float MageDamage = (1 + tempMageAttribute.Intelligence / 100) * weapon.WeaponDamage;
            return MageDamage;
        }

        // method for displaying the status of the mage
        public string Display(Weapon weapon)
        {
            HeroAttribute attributes = TotalAttributes();
            float damage = Damage(weapon);
            string display = String.Format("Name: {0}\nClass: Mage\nPlayer level: {1}\nTotal strength: {2}\nTotal dexterity: {3}\nTotal intelligence: {4}\nDamage: {5}", Name, PlayerLevel, attributes.Strength, attributes.Dexterity, attributes.Intelligence, damage);
            return display;
        }
    }
}
