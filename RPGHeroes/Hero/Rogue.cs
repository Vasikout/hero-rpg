﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGHeroes
{
    public class Rogue : Hero
    {
        public Rogue(string name)
        {
            LevelAttributes = new HeroAttribute();
            Name = name;
            LevelAttributes.Strength = 2;
            LevelAttributes.Dexterity = 6;
            LevelAttributes.Intelligence = 1;
            PlayerLevel = 1;
        }

        public List<Weapon.WeaponType> weapons = new List<Weapon.WeaponType>();
        public List<Armor.ArmorType> armors = new List<Armor.ArmorType>();


        public override void LevelUp()
        {
            PlayerLevel = PlayerLevel + 1;
            LevelAttributes.IncreaseStrength(1);
            LevelAttributes.IncreaseDexterity(4);
            LevelAttributes.IncreaseIntelligence(1);
        }

        public void EquipWeapon(Weapon weapon)
        {
            if ((weapon.weaponType != Weapon.WeaponType.Daggers && weapon.weaponType != Weapon.WeaponType.Swords) || weapon.RequiredLevel > PlayerLevel)
            {
                throw new InvalidWeaponException("You can't equip this weapon");
            }
            else
            {
                Equipment.Add(Item.Slot.Weapon, weapon);
                Console.WriteLine("This is the correct weapon");
            };
        }

        public void EquipArmor(Armor armor)
        {
            if ((armor.armorType != Armor.ArmorType.Leather && armor.armorType != Armor.ArmorType.Mail) || armor.RequiredLevel > PlayerLevel)
            {
                throw new InvalidArmorException("You cant equip this armor");
            }
            else
            {
                Equipment.Add(Item.Slot.Body, armor);
                Console.WriteLine("This is the correct armor");
            }
        }

        public override HeroAttribute TotalAttributes()
        {
            HeroAttribute outputHeroAttribute = new HeroAttribute();
            foreach (var item in Equipment)
            {
                if (item.Key == Item.Slot.Weapon)
                {
                    continue;
                }
                if(item.Value == null)
                {
                    continue;
                }
                outputHeroAttribute.Dexterity += item.Value.ArmorAttribute.Dexterity;
                outputHeroAttribute.Intelligence += item.Value.ArmorAttribute.Intelligence;
                outputHeroAttribute.Strength += item.Value.ArmorAttribute.Strength;
            }
            outputHeroAttribute.Dexterity += LevelAttributes.Dexterity;
            outputHeroAttribute.Intelligence += LevelAttributes.Intelligence;
            outputHeroAttribute.Strength += LevelAttributes.Strength;

            return outputHeroAttribute;

        }

        public float Damage(Weapon weapon)
        {
            HeroAttribute tempRogueAttribute = TotalAttributes();
            float RogueDamage = (1 + tempRogueAttribute.Dexterity / 100) * weapon.WeaponDamage;
            return RogueDamage;
        }

        /// <summary>
        /// this function displays the characteristics of the class Rogue
        /// </summary>
        /// <param name="weapon">the weapon to calculate the total damage</param>
        /// <returns>a string with all the characteristics</returns>
        public string Display(Weapon weapon)
        {
            HeroAttribute attributes = TotalAttributes();
            float damage = Damage(weapon);
            string display = String.Format("Name: {0}\nClass: Rogue\nPlayer level: {1}\nTotal strength: {2}\nTotal dexterity: {3}\nTotal intelligence: {4}\nDamage: {5}", Name, PlayerLevel, attributes.Strength, attributes.Dexterity, attributes.Intelligence, damage);
            return display;
        }
    }
}
