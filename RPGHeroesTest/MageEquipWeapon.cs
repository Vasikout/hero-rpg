﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using RPGHeroes;

namespace RPGHeroesTest
{
    public class MageEquipWeapon
    {
        [Fact]

        public void MageEquipAppropriateWeaponOrException()
        {
            Mage mage = new Mage("Nick");
            Weapon weapon = new Weapon(Weapon.WeaponType.Staffs, "Stuff", Item.Slot.Weapon);
            mage.EquipWeapon(weapon);
            int actualLevel = weapon.RequiredLevel = 1;

            Weapon.WeaponType expectedType = Weapon.WeaponType.Staffs;
            int expectedLevel = 1;

            Assert.Equal(expectedType, weapon.weaponType);
            Assert.Equal(expectedLevel, actualLevel);


        }
    }
}
