﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RPGHeroes;
using Xunit;

namespace RPGHeroesTest
{
    public class RogueHeroCreation
    {
        [Fact]
        public void CreationRogueCorrectLevelNameAttributes()
        {
            Rogue rogue = new Rogue("Jared");

            HeroAttribute rogueAttr = rogue.LevelAttributes;
            //Act
            int expectedStrength = 2;
            int expectedDexterity = 6;
            int expectedIntelligence = 1;
            int expectedLevel = 1;
            string expectedName = "Jared";

            //Assert

            Assert.Equal(expectedName, rogue.Name);
            Assert.Equal(expectedLevel, rogue.PlayerLevel);
            Assert.Equal(expectedIntelligence, rogueAttr.Intelligence);
            Assert.Equal(expectedDexterity, rogueAttr.Dexterity);
            Assert.Equal(expectedStrength, rogueAttr.Strength);
        }
    }
}
