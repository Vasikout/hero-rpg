﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGHeroes
{
    public abstract class Hero
    {
        //constructor of hero class initiating the dictionary Equipment
        public Hero()
        {
            Equipment.Add(Item.Slot.Weapon, null);
            Equipment.Add(Item.Slot.Head, null);
            Equipment.Add(Item.Slot.Body, null);
            Equipment.Add(Item.Slot.Legs, null);
        }

        public string Name { get; set; }

        private int Level;
        
        public int PlayerLevel
        {
            get
            {
                return Level;
            }

            set
            {
                Level = value;
            }
        }

        public HeroAttribute LevelAttributes { get; set; }

        public Dictionary<Item.Slot, Item> Equipment = new Dictionary<Item.Slot, Item>();

        public List<string> ValidWeaponTypes = new List<string>();
        public List<string> ValidArmorTypes = new List<string>();


        public abstract void LevelUp();


        public void EquipArmor()
        {

        }
        

        public void EquipWeapon()
        {

        }

        public void Damage()
        {
        }

        public abstract HeroAttribute TotalAttributes();
        

        public void Display()
        {
        }
    }
}
