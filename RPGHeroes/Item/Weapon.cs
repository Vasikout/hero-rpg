﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGHeroes
{
    public class Weapon: Item
    {

        public WeaponType weaponType;
        public Slot WeaponSlot;
        public string WeaponName;
        public Weapon(WeaponType weaponType, string WeaponName, Slot WeaponSlot)
        {
            this.WeaponName = WeaponName;
            this.weaponType = weaponType;
            WeaponSlot = Slot.Weapon;
            switch (weaponType)
            {
                case WeaponType.Axes:
                    WeaponDamage = 3; 
                    break;
                case WeaponType.Bows:
                    WeaponDamage = 2;
                    break;
                case WeaponType.Daggers:
                    WeaponDamage = 1;
                    break;
                case WeaponType.Hammers:
                    WeaponDamage = 4;
                    break;
                case WeaponType.Staffs:
                    WeaponDamage = 3;
                    break;
                case WeaponType.Swords:
                    WeaponDamage = 4;
                    break;
                case WeaponType.Wands:
                    WeaponDamage = 3;
                    break;
            }
        }

        public enum WeaponType
        {
            Axes,
            Bows,
            Daggers,
            Hammers,
            Staffs,
            Swords,
            Wands
        }

        public int WeaponDamage { get; set; }

        
    }
}
